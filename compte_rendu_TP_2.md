#Compte rendu TP 2

#I/
#```bash
#install mariadb

$dnf install mariadb-server
[...]
Complete!

#start service Mariadb + status:

$[root@db ~]# systemctl start mariadb
$[root@db ~]# systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 15:05:46 CET; 8s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 4676 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 4541 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 4517 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 4644 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 4956)
   Memory: 82.9M
   CGroup: /system.slice/mariadb.service
           └─4644 /usr/libexec/mysqld --basedir=/usr

[...]
```

```bash

#config start mariadb au démarrage de la machine

$[root@db ~]# sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```
```bash
#connaitre le port d'écoute de MariaDB:

$[root@db ~]# ss -tlpn
State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port    Process
[...]
LISTEN    0         80                       *:3306                  *:*        users:(("mysqld",pid=4644,fd=21))
[...]
```

```bash
#Processus lié à MariaDB: (grep cherche un mot clé)

$[root@db ~]# ps -ef | grep  mysqld
mysql       4644       1  0 15:05 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
root        4870    1497  0 15:50 pts/0    00:00:00 grep --color=auto mysqld
```

```bash
#Savoir quel utilisateur à lancer le service mariadb:
$[root@db ~]# ps -u | grep mysql
root        4894  0.0  0.1 221928  1116 pts/0    S+   15:58   0:00 grep --color=auto mysql
```

```bash
[root@db ~]# firewall-cmd --permanent --add-port=3306/tcp
success


[root@db ~]# mysql_secure_installation

[...]
All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!

```

```bash

#**CONNEXION A MariaDB AVEC SON ROOT :

[root@db ~]# sudo mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 15
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>


```



```bash
#**CREATION USER

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'toor';
Query OK, 0 rows affected (0.000 sec)
```


```bash
#creation BDD pour user nextcloud

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)


#Attribution de tous les droits pour nextcloud

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.000 sec)

#actualisation des privileges

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

```

```bash
#installation commande mysql
[root@web ~]# dnf provides mysql
Last metadata expiration check: 1 day, 4:07:16 ago on Tue 14 Dec 2021 03:35:44 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : @System
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[root@web ~]# dnf install mysql
 [...]
Complete!
````

```bash

#connexion de web à db :

[root@web ~]# mysql -h 10.2.1.12 -u nextcloud -p nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 16
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.


mysql> SHOW TABLES;
Empty set (0.00 sec)

mysql>
```

```bash
#II/
#1.A
#installation et démarrage apache:

$ dnf install httpd
[...]
Complete!

[root@web ~]# systemctl start httpd
[root@web ~]# systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[root@web ~]#
```

```bash
#Processus liés:

[root@web ~]# ps -ef | grep  httpd
root        2415       1  0 20:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2416    2415  0 20:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2417    2415  0 20:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2418    2415  0 20:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2419    2415  0 20:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
root        2651    1453  0 20:58 pts/0    00:00:00 grep --color=auto httpd
[root@web ~]#

```

```bash
#Port d'écoute:
[root@web ~]# ss -tlpn
State    Recv-Q   Send-Q     Local Address:Port     Peer Address:Port  Process
[...]
LISTEN   0        128                    *:80                  *:*      users:(("httpd",pid=2419,fd=4),("httpd",pid=2418,fd=4),("httpd",pid=2417,fd=4),("httpd",pid=2415,fd=4))
[root@web ~]#
```


```bash
#Utilisateur using process:

[root@web ~]# ps -u | grep httpd
root        2659  0.0  0.1 221928  1152 pts/0    S+   21:01   0:00 grep --color=auto httpd
```

```bash
#PREMIER TEST

[root@web ~]# firewall-cmd --permanent --add-port=80/tcp
[root@web ~]# firewall-cmd --reload
[root@web ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[root@web ~]#
```

```bash
#TEST ACCES DEPUIS HOTE:

C:\Users\louis>curl 10.2.1.11:80
<!doctype html>
<html>
 [...]
  </body>
</html>

http://10.2.1.11/
test sur web : OK Titre page "HTTP Server Test Page"
```

```bash
#B.
#install php

# ajout des dépôts EPEL
$ sudo dnf install epel-release
$ sudo dnf update
# ajout des dépôts REMI
$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
$ dnf module enable php:remi-7.4

# install de PHP et de toutes les libs PHP requises par NextCloud
$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp


#2.conf apache:
[root@web ~]# cd /etc/httpd/conf

#Contenu fichier de conf apache:
[root@web httpd]# cd conf.d
[root@web conf.d]# ls
autoindex.conf  php74-php.conf  README  userdir.conf  welcome.conf

#Creation virtualhost:

[root@web conf.d]# touch vitualhost.conf
[root@web conf.d]# ls
autoindex.conf  php74-php.conf  README  userdir.conf  vitualhost.conf  welcome.conf


#config racine
[root@web www]# mkdir nextcloud
[root@web www]# ls
cgi-bin  html  nextcloud
[root@web www]# cd nextcloud/
[root@web nextcloud]# mkdir html
[root@web nextcloud]# ls
html

#droit users pour futur lancement:
[root@web www]# chmod 777 nextcloud nextcloud/



#Config PHP
/etc/opt/remi/php74/php.ini
[root@web php74]# nano php.ini

date.timezone = "Europe/Paris



#NextCloud:
curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip

[root@web ~]# unzip nextcloud-21.0.1.zip
[root@web ~]# ls
anaconda-ks.cfg  nextcloud  nextcloud-21.0.1.zip
[root@web ~]# rm nextcloud-21.0.1.zip



#4.Test

#modif fichier host:
PS C:\Windows\System32\drivers\etc> notepad hosts
10.2.1.11 web.tp2.cesi

#test accès nextcloud:

[root@web nextcloud]$ cp -R ./* /var/www/nextcloud/html/
[root@web html]$ sudo chown apache:apache /var/www/nextcloud/html/*

```



























