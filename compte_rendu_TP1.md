note rocky linux :

www.       google.fr
hostname   DomainName


Connexion SSH:
ssh root@192.168.56.20

Soleil = présent dans le compte rendu
exemple : ajout dns (copie des commandes) + commandes de vérif



:qa! <-- 













**#Compte rendu tp1:


#PART1_______________:

#groupe Wheel:

visudo /etc/sudoers

bash```
[...]

## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL

## Same thing without a password
# %wheel        ALL=(ALL)       NOPASSWD: ALL

[...]

```


#création d'un nouvel user:


bash```
---création et mot de passe:----
[root@node1 ~]# sudo useradd luigi
[root@node1 ~]# sudo passwd luigi
Changing password for user luigi.
web password:
BAD PASSWORD: The password is shorter than 8 characters
Retype web password:
passwd: all authentication tokens updated successfully.
[root@node1 ~]#
```

#-------il doit avoir un répertoire personnel (homedir) dans /home/<USER>------
bash```

[root@node1 ~]# sudo mkhomedir_helper luigi
[root@node1 ~]# ls -al /home/luigi
total 16
drwx------. 2 luigi luigi  83 Dec 13 13:48 .
drwxr-xr-x. 4 root  root   31 Dec 13 13:33 ..
-rw-------. 1 luigi luigi  26 Dec 13 13:53 .bash_history
-rw-r--r--. 1 luigi luigi  18 Jul 27 16:21 .bash_logout
-rw-r--r--. 1 luigi luigi 141 Jul 27 16:21 .bash_profile
-rw-r--r--. 1 luigi luigi 376 Jul 27 16:21 .bashrc
[root@node1 ~]# cd /home
[root@node1 home]# ls
luigi  toto
```

#----------------------il doit appartenir au groupe wheel--------------
bash```
[root@node1 ~]# usermod -aG wheel luigi
[root@node1 ~]# id -Gn luigi  ou [root@node1 ~]# groups luigi
luigi wheel
```


#------une fois créé, connectez-vous avec, et vérifiez que vous pouvez 
bash```
exécuter des commandes avec sudo---------------------

[root@node1 ~]# su - luigi
[luigi@node1 ~]$ sudo ls -la /root

We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.

[sudo] password for luigi:
total 28
dr-xr-x---.  2 root root  135 Dec 13 11:39 .
dr-xr-xr-x. 17 root root  224 Dec 13 10:32 ..
-rw-------.  1 root root 1348 Dec 13 10:38 anaconda-ks.cfg
-rw-------.  1 root root  460 Dec 13 13:22 .bash_history
-rw-r--r--.  1 root root   18 Mar 14  2021 .bash_logout
-rw-r--r--.  1 root root  176 Mar 14  2021 .bash_profile
-rw-r--r--.  1 root root  176 Mar 14  2021 .bashrc
-rw-r--r--.  1 root root  100 Mar 14  2021 .cshrc
-rw-r--r--.  1 root root  129 Mar 14  2021 .tcshrc
[luigi@node1 ~]$
```

#PART2:____________________
bash```
 1.
statut service SSH:

[root@node1 ~]# systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-13 11:56:12 CET; 2h 7min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 858 (sshd)
    Tasks: 1 (limit: 4943)
   Memory: 3.9M
   CGroup: /system.slice/sshd.service
           └─858 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc>

Dec 13 13:55:32 node1.tp1.cesi sshd[1731]: pam_unix(sshd:session): session opened for user root by (uid=0)
Dec 13 13:55:50 node1.tp1.cesi sshd[1758]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh r>
Dec 13 13:55:52 node1.tp1.cesi sshd[1758]: Failed password for root from 192.168.56.1 port 65485 ssh2
Dec 13 13:55:54 node1.tp1.cesi sshd[1758]: fatal: userauth_finish: Connection reset by peer [preauth]
Dec 13 13:56:04 node1.tp1.cesi sshd[1761]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh r>
Dec 13 13:56:06 node1.tp1.cesi sshd[1761]: Failed password for luigi from 192.168.56.1 port 65487 ssh2
Dec 13 13:56:12 node1.tp1.cesi sshd[1761]: Accepted password for luigi from 192.168.56.1 port 65487 ssh2
Dec 13 13:56:12 node1.tp1.cesi sshd[1761]: pam_unix(sshd:session): session opened for user luigi by (uid=0)
Dec 13 13:58:47 node1.tp1.cesi sshd[1810]: Accepted password for root from 192.168.56.1 port 65520 ssh2
Dec 13 13:58:47 node1.tp1.cesi sshd[1810]: pam_unix(sshd:session): session opened for user root by (uid=0)
lines 1-21/21 (END)
[...]
```


bash```
#Port SSH:

[root@node1 ~]# ss
[...]
tcp   ESTAB  0      0            192.168.56.20:ssh        192.168.56.1:65520
[root@node1 ~]# 




sudo ss -lutpn:

-l, --listening : Display only listening sockets (these are omitted by default).

-u, --udp : Display UDP sockets.

-t, --tcp : Display TCP sockets.

-p, --processes : Show process using socket.

-n, --numeric
Do not try to resolve service names. Show exact bandwidth values, instead of human-readable.

[root@node1 ~]# sudo ss -lutpn
Netid    State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port   Process
tcp      LISTEN    0         128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=858,fd=5))
tcp      LISTEN    0         128                   [::]:22                 [::]:*       users:(("sshd",pid=858,fd=7))
[root@node1 ~]#

[root@node1 ~]# ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
[...]
root        1810     858  0 13:58 ?        00:00:00 sshd: root [priv]
root        1814    1810  0 13:58 ?        00:00:00 sshd: root@pts/0
[...]
```

#2.
#Changez le port d'écoute du service SSH :
bash```
[root@node1 ~]# sudo nano /etc/ssh/sshd_config

[...]
Port 2222 (j'ai changé le #Port 22 par défaut par Port 2222, de plus il est conseillé de prendre des ports libres entre 1024 et 65535)
[...]

RESTART SSH et STATUT:

[root@node1 ~]# systemctl restart sshd
[root@node1 ~]# systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-13 14:33:25 CET; 20s ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 1923 (sshd)
    Tasks: 1 (limit: 4943)
   Memory: 1.1M
   CGroup: /system.slice/sshd.service
           └─1923 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256>

Dec 13 14:33:25 node1.tp1.cesi systemd[1]: sshd.service: Succeeded.
Dec 13 14:33:25 node1.tp1.cesi systemd[1]: Stopped OpenSSH server daemon.
Dec 13 14:33:25 node1.tp1.cesi systemd[1]: Starting OpenSSH server daemon...
Dec 13 14:33:25 node1.tp1.cesi sshd[1923]: Server listening on 0.0.0.0 port 22.
Dec 13 14:33:25 node1.tp1.cesi sshd[1923]: Server listening on :: port 22.
Dec 13 14:33:25 node1.tp1.cesi systemd[1]: Started OpenSSH server daemon.
lines 1-17/17 (END)
[...]
```



#Firewall config:
bash```
[root@node1 ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[root@node1 ~]# firewall-cmd --permanent --add-port=2222/tcp
success

[root@node1 ~]# firewall-cmd --reload
success

[root@node1 ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 2222/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[root@node1 ~]#

 Vérif:
 
[root@node1 ~]# sudo ss -lutpn
Netid   State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port   Process
tcp     LISTEN    0         128                0.0.0.0:2222            0.0.0.0:*       users:(("sshd",pid=2040,fd=5))
tcp     LISTEN    0         128                   [::]:2222               [::]:*       users:(("sshd",pid=2040,fd=7))
[root@node1 ~]#
```

#3.
bash```
[root@node1 ~]# sudo dnf update
[...]
Complete!

[root@node1 ~]# sudo dnf install nginx
[...]
Complete!

ACTIVATION ET STATUT:

[root@node1 ~]# systemctl start nginx
[root@node1 ~]# systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-13 15:11:48 CET; 9s ago
  Process: 37157 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 37156 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 37154 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 37159 (nginx)
    Tasks: 2 (limit: 4943)
   Memory: 3.7M
   CGroup: /system.slice/nginx.service
           ├─37159 nginx: master process /usr/sbin/nginx
           └─37160 nginx: worker process

Dec 13 15:11:48 node1.tp1.cesi systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 13 15:11:48 node1.tp1.cesi nginx[37156]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 13 15:11:48 node1.tp1.cesi nginx[37156]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 13 15:11:48 node1.tp1.cesi systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argument
Dec 13 15:11:48 node1.tp1.cesi systemd[1]: Started The nginx HTTP and reverse proxy server.
[root@node1 ~]#
```

#PORT Nginx:
bash```

[root@node1 ~]# sudo ss -lutpn
Netid        State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port        Process
tcp          LISTEN        0             128                        0.0.0.0:2222                    0.0.0.0:*            users:(("sshd",pid=2040,fd=5))
tcp          LISTEN        0             128                        0.0.0.0:80                      0.0.0.0:*            users:(("nginx",pid=37160,fd=8),("nginx",pid=37159,fd=8))
tcp          LISTEN        0             128                           [::]:2222                       [::]:*            users:(("sshd",pid=2040,fd=7))
tcp          LISTEN        0             128                           [::]:80                         [::]:*            users:(("nginx",pid=37160,fd=9),("nginx",pid=37159,fd=9))
[root@node1 ~]#

ajout dans Firewall:
[root@node1 ~]# firewall-cmd --permanent --add-port=80/tcp
success
[root@node1 ~]# firewall-cmd --reload
success
[root@node1 ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 2222/tcp 80/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[root@node1 ~]#


(VOIR SCREEN EN PLUS)
[root@node1 ~]# curl 192.168.56.20

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    [...]
      </div>
    </div>
  </body>
</html>
```

#4.
bash```
[root@node1 ~]# dnf install python3
Complete!

[root@node1 ~]# firewall-cmd --permanent --add-port=8888/tcp
success
[root@node1 ~]# firewall-cmd --reload
success
[root@node1 ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 2222/tcp 80/tcp 8888/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[root@node1 ~]#

accès web :
192.168.56.20:8888

[root@node1 ~]# python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
192.168.56.1 - - [13/Dec/2021 15:46:49] "GET / HTTP/1.1" 200 -
192.168.56.1 - - [13/Dec/2021 15:46:49] code 404, message File not found
192.168.56.1 - - [13/Dec/2021 15:46:49] "GET /favicon.ico HTTP/1.1" 404 -

CURL ACCES:

C:\Users\louis>curl 192.168.56.20:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href=".bash_history">.bash_history</a></li>
<li><a href=".bash_logout">.bash_logout</a></li>
<li><a href=".bash_profile">.bash_profile</a></li>
<li><a href=".bashrc">.bashrc</a></li>
<li><a href=".cshrc">.cshrc</a></li>
<li><a href=".tcshrc">.tcshrc</a></li>
<li><a href="anaconda-ks.cfg">anaconda-ks.cfg</a></li>
</ul>
<hr>
</body>
</html>
```

#création d'un service:
bash```
[root@node1 system]# touch web.service
[root@node1 system]# ls
basic.target.wants                          default.target.wants         sockets.target.wants
ctrl-alt-del.target                         getty.target.wants           sysinit.target.wants
dbus-org.fedoraproject.FirewallD1.service   multi-user.target.wants      syslog.service
dbus-org.freedesktop.nm-dispatcher.service  network-online.target.wants  systemd-timedated.service
dbus-org.freedesktop.timedate1.service      web.service                  timers.target.wants
default.target                              nginx.service.d
[root@node1 system]# nano web.service
[root@node1 system]# [root@node1 system]# nano web.service

DANS FICHIER web.service :

[Unit]
Description=camarche

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target



Relecture des fichiers par systemd (A chaque modif d'un fichier service):

[root@node1 system]# sudo systemctl daemon-reload



STATUS:

[root@node1 system]# sudo systemctl status web.service
● web.service - camarche
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-13 16:17:23 CET; 12s ago
 Main PID: 37796 (python3)
    Tasks: 1 (limit: 4943)
   Memory: 9.4M
   CGroup: /system.slice/web.service
           └─37796 /usr/bin/python3 -m http.server 8888

Dec 13 16:17:23 node1.tp1.cesi systemd[1]: Started camarche.
[root@node1 system]#


START:

[root@node1 system]# sudo systemctl start web.service

ENABLE :

[root@node1 system]# sudo systemctl enable web.service
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
[root@node1 system]#




C.
[root@node1 system]# sudo useradd web


[root@node1 ~]# cd /srv
[root@node1 srv]# mkdir web
[root@node1 srv]# ls
web

```

Pas fini
utilisez une commande chown pour que ce dossier et le fichier appartiennent à l'utilisateur web
























 
 